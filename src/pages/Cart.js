import React from "react";

import Icon from "../components/Icon";
import PageWrapper from "../components/PageWrapper";
import CartItem from "../components/CartItem";

import { useSelector } from "react-redux";

const Cart = () => {
    const cart = useSelector((store) => store.cart);

    return (
        <PageWrapper className="d-flex flex-column align-items-center">
            <h3 className="py-2">
                <Icon icon="shopping-cart" className="me-2" />
                Cart
            </h3>
            {cart.length === 0 ? (
                <h5>No Item 😭!</h5>
            ) : (
                <>
                    <div className="w-cart-container">
                        {cart.map((item) => (
                            <CartItem item={item} key={item.id} />
                        ))}
                    </div>
                    <h4>
                        Total:{" "}
                        {cart.reduce(
                            (sum, item) => sum + item.price * item.quantity,
                            0
                        )}
                        <img
                            src="/img/Emerald.png"
                            alt="Emerald"
                            style={{ height: 24, width: 24 }}
                        />
                    </h4>
                </>
            )}
        </PageWrapper>
    );
};

export default Cart;
