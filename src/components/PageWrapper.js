import React from "react";

const PageWrapper = ({ children, className }) => {
    return (
        <div
            className={`text-center ${className}`}
            style={{ paddingTop: "5rem", minHeight: "100vh" }}
        >
            {children}
        </div>
    );
};

export default PageWrapper;
