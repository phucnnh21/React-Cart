export const products = [
    { id: 1, name: "Diamond Boots", img: "img/Diamond_Boots.png", price: 4 },
    {
        id: 2,
        name: "Diamond Chestplate",
        img: "img/Diamond_Chestplate.png",
        price: 8,
    },
    { id: 3, name: "Diamond Helmet", img: "img/Diamond_Helmet.png", price: 5 },
    {
        id: 4,
        name: "Diamond Leggings",
        img: "img/Diamond_Leggings.png",
        price: 7,
    },
    {
        id: 5,
        name: "Diamond Pickaxe",
        img: "img/Diamond_Pickaxe.png",
        price: 3,
    },
    { id: 6, name: "Diamond Sword", img: "img/Diamond_Sword.png", price: 2 },
    { id: 7, name: "Diamond", img: "img/Diamond.png", price: 1 },
    {
        id: 8,
        name: "Enhanted Golden Apple",
        img: "img/Enchanted_Golden_Apple.gif",
        price: 39,
    },
    {
        id: 9,
        name: "Elytra",
        img: "img/Elytra.png",
        price: 9,
    },
    {
        id: 10,
        name: "CreamPie",
        img: "img/Cake.png",
        price: 9,
    },
    {
        id: 11,
        name: "Emerald",
        img: "img/Emerald.png",
        price: 3,
    },
    {
        id: 12,
        name: "Kama Sutra",
        img: "img/Book.png",
        price: 69,
    },
];
