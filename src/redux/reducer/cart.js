import { ADD, REMOVE, UPDATE } from "../action";

// store.cart
const initialState = [];

const cartReducer = (state = initialState, action) => {
    switch (action.type) {
        // action = { type: ADD, payload: product }
        case ADD: {
            if (state.some((item) => item.id === action.payload.id)) {
                return state;
            }

            return [...state, { ...action.payload, quantity: 1 }];
        }

        // action = { type: REMOVE, payload: productId }
        case REMOVE: {
            return state.filter((item) => item.id !== action.payload);
        }

        // action = { type: UPDATE, payload: { productId, value } }
        case UPDATE: {
            return state.map((item) => {
                if (item.id === action.payload.productId) {
                    return {
                        ...item,
                        quantity: item.quantity + action.payload.value,
                    };
                }

                return item;
            });
        }

        default:
            return state;
    }
};

export default cartReducer;
