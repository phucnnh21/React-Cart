import { combineReducers } from "redux";

import cartReducer from "./cart";
// import darkmodeReducer from  "./darkmode";

const combinedReducers = combineReducers({
    cart: cartReducer,
    // isDarkmode: darkmodeReducer,
});

export default combinedReducers;
