export const ADD = "cart/addToCart";
export const REMOVE = "cart/removeFromCart";
export const UPDATE = "cart/updateQuantity";
// const LOGIN = "user/login";

// export const login = (data) => {
//     localStorage.setItem("user", data);

//     return { type: LOGIN, payload: data };
// };
