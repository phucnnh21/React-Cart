import { ADD, REMOVE, UPDATE } from "./action";

const initialState = {
    cart: [],
    isDarkMode: false,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        // action = { type: ADD, payload: product }
        case ADD: {
            if (state.cart.some((item) => item.id === action.payload.id)) {
                return state;
            }

            return {
                ...state,
                cart: [...state.cart, { ...action.payload, quantity: 1 }],
            };
        }

        // action = { type: REMOVE, payload: productId }
        case REMOVE: {
            return {
                ...state,
                cart: state.cart.filter((item) => item.id !== action.payload),
            };
        }

        // action = { type: UPDATE, payload: { productId, value } }
        case UPDATE: {
            return {
                ...state,
                cart: state.cart.map((item) => {
                    if (item.id === action.payload.productId) {
                        return {
                            ...item,
                            quantity: item.quantity + action.payload.value,
                        };
                    }

                    return item;
                }),
            };
        }

        default:
            return state;
    }
};

export default reducer;
